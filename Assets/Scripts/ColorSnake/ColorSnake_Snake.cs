﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ColorSnake_Snake : MonoBehaviour
{
    [SerializeField] private ColorSnake_GameController m_GameController;
    
    [SerializeField] private SpriteRenderer m_SpriteRender;
    private Vector3 position;
    [SerializeField] private Text m_Text;
    public static int score;

    private int currentType;
    
    private void Start()
    {
        position = transform.position;
        var colorType = m_GameController.Types.GetRandomColorType();
        currentType = colorType.Id;
        m_SpriteRender.color = colorType.Color;
        m_Text.text = $"Количество очков {score}";
    }

    // Update is called once per frame
    private void Update()
    {
        position = transform.position;

        if (!Input.GetMouseButton(0))
        {
            return;
        }

        position.x = m_GameController.Camera.ScreenToWorldPoint(Input.mousePosition).x;
        
        position.x = Mathf.Clamp(position.x, m_GameController.Bounds.Left, m_GameController.Bounds.Right);

        transform.position = position;
        m_Text.text = $"Количество очков {score}";
    }

    private void SetupColor(int id)
    {

        var colorType = m_GameController.Types.GetColorType(id);
        var objectType = m_GameController.Types.GetObjectType(id);
        currentType = colorType.Id;
        m_SpriteRender.color = colorType.Color;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var obstacle = other.gameObject.GetComponent<ColorSnake_Obstacle>();
        var objId = other.gameObject.GetComponent<ColorSnake_Types>();        
               
        if (obstacle == null) 
        {
            return;
        }
        if (other.gameObject.CompareTag("ChangeColor"))
        {
            SetupColor(obstacle.ColorId);
            Destroy(obstacle.gameObject);
        }
        if (currentType == obstacle.ColorId)
        {
            score++;
            Debug.Log(score);
            SetupColor(obstacle.ColorId);
            Destroy(obstacle.gameObject);
        }
        else
        {
            SceneManager.LoadScene("ColorSnake_Finish");
        }
    }
}
