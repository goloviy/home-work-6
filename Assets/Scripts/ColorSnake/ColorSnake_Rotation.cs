﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSnake_Rotation : MonoBehaviour
{
    [SerializeField] private GameObject m_obj;

    // Update is called once per frame
    void Update()
    {
        m_obj.transform.Rotate(new Vector3(0, 0, 45) * Time.deltaTime);
    }
}
