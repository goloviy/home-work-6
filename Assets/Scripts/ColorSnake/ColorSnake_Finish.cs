﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSnake_Finish : MonoBehaviour
{
    [SerializeField] private Text m_Text;
    
    void Start()
    {
        m_Text.text = $"Количество очков = {ColorSnake_Snake.score}";
    }
}
