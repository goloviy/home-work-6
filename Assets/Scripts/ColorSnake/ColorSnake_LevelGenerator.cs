﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSnake_LevelGenerator : MonoBehaviour
{
    [SerializeField] private ColorSnake_Types m_Types;

    [SerializeField] private ColorSnake_GameController m_Controller;
    
    private int line = 1; // номер генерируемоко чанка
    private List<GameObject> obstacles = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        var upBorder = m_Controller.Bounds.Up;

        while (line * 2 < upBorder *2f)
        {
            GenerateObstacle();
        }
    }

    // Update is called once per frame
    void Update()
    {
        var upBorder = m_Controller.Bounds.Up + m_Controller.Camera.transform.position.y;
        if (line * 2 > upBorder * 2)
        {
            return;
        }
        GenerateObstacle();
        
        //TODO уничтожение нижних объектов написать самостоятельно
    }

    private void GenerateObstacle()
    {
        var template = m_Types.GetRandomTemplate();      
        var  obstacle = new GameObject($"Obstacle{line}");

        //foreach (var point in template.Points)
        //{
        //    var objType = m_Types.GetRandomObjectType();
        //    //if (objType == m_Types.GetObjectType(4))
        //    //{
        //    //    Debug.Log("Работает");
        //    //    continue;
        //    //}
        //    var colorType = m_Types.GetRandomColorType();

        //    var obj = Instantiate(objType.Object, point.position, point.rotation);
        //    obj.transform.parent = obstacle.transform;

        //    obj.GetComponent<SpriteRenderer>().color = colorType.Color;

        //    var obstacleComponent = obj.AddComponent<ColorSnake_Obstacle>();
        //    obstacleComponent.ColorId = colorType.Id;
        //}

        for (int i = 0; i < template.Points.Length; i++)
        {
            var point = template.Points[i];
            var objType = m_Types.GetRandomObjectType();
            if (i == 4)
            {                
                var colorType2 = m_Types.GetRandomColorType();
                var objColorChange = m_Types.GetObjectType(4);
                var obj2 = Instantiate(objColorChange.Object, template.Points[4].position, template.Points[4].rotation);
                obj2.transform.parent = obstacle.transform;                

                obj2.GetComponent<SpriteRenderer>().color = colorType2.Color;

                var obstacleComponent2 = obj2.AddComponent<ColorSnake_Obstacle>();
                obstacleComponent2.ColorId = colorType2.Id;                
                continue;
            }            
            var colorType = m_Types.GetRandomColorType();

            var obj = Instantiate(objType.Object, point.position, point.rotation);
            obj.transform.parent = obstacle.transform;

            obj.GetComponent<SpriteRenderer>().color = colorType.Color;

            var obstacleComponent = obj.AddComponent<ColorSnake_Obstacle>();
            obstacleComponent.ColorId = colorType.Id;
        }

        Vector3 pos = obstacle.transform.position;
        pos.y = line * 2;

        obstacle.transform.position = pos;

        line++;        
        obstacles.Add(obstacle);
    }
}
